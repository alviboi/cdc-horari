<?php

namespace App\Http\Controllers;

use App\Models\compensa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Jobs\SendAviscompensacio;
use App\Mail\Aviscompensacio;
use Carbon\Carbon;

class compensaController extends Controller
{
    /**
     * Mostra un llistat de tot el recurs
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return compensa::get();
    }

    /**
     * Extrau totes les dades de fitxar del compensa amb el nom
     *
     * @return \Illuminate\Http\Response
     */
    public function get_data_index($any, $mes)
    {
        $ret = array();
        $els = compensa::whereMonth('data', '=', date($mes))->whereYear('data', '=', date($any))->get();
        foreach ($els as $el) {
            $item=array("id"=>$el->id, "name"=>$el->user['name'], "data"=>$el->data, "inici"=>$el->inici, "fi"=>$el->fi, "motiu"=>$el->motiu);
            array_push($ret, $item);
        }
        return $ret;
    }



    /**
     * Crea un element del recurs
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Guarda l'elememt creat.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $dat = new compensa();
        $dat->data = $request->data;
        $dat->inici = $request->inici;
        $dat->fi = $request->fi;
        $dat->user_id = auth()->id();
        $dat->motiu = $request->motiu;
        $dat->save();
        return $dat->toArray();
    }



    /**
     * Actualitza l'element del recurs a la base de dades
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\compensa  $compensa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, compensa $compensa)
    {
        //
    }

    /**
     * Elimina l'element  del recurs de la base de dades
     *
     * @param  \App\Models\compensa  $compensa
     * @return \Illuminate\Http\Response
     */
    public function destroy(compensa $compensa)
    {
        //
        
        if($compensa->inici == "09:00:00"){
            $m='matí';
        }else{
            $m='vesprada';
        }

        $link2 = "https://calendar.google.com/calendar/u/0/r/eventedit?text=COMPENSACIÓ+CEFIRE+ELIMINADA&dates=" . str_replace("-", "", $compensa->data) . "T" . str_replace(":", "", $compensa->inici) . "/" . str_replace("-", "", $compensa->data) . "T" . str_replace(":", "", $compensa->fi) . "&details=compensa+del+Cefire+de+Valencia+ELIMINADA&location=Valencia&trp=false#eventpage_6";
        $link3= "https://outlook.office.com/calendar/0/deeplink/compose?body=Guardia%20al%20CEFIRE&enddt=".$compensa->data."T".str_replace(":", "%3A", $compensa->fi)."&location=Valencia&path=%2Fcalendar%2Faction%2Fcompose&rru=addevent&startdt=".$compensa->data."T".str_replace(":", "%3A", $compensa->inici)."&subject=GUARDIA%20CEFIRE%20VALENCIA";

        $datos2 = [
            'nombre' => $compensa->user['name'],
            'fecha' => date("d/m/Y", strtotime($compensa->data)),
            'rato' => $m,
            'link' => $link2,
            'link2' => $link3
            'estat' => 'Eliminada'
        ];


        $emailJob2 = (new SendAviscompensacio($compensa->user['email'], $datos2))->delay(Carbon::now()->addSeconds(120));
        dispatch($emailJob2);

        $compensa->delete();
    }

    public function compensacionsnovalidades()
    {
        //
        $ret = array();
        $els = compensa::where('confirmat','=',false)->get();
        $dias=array("Diumenge","Dilluns","Dimarts","Dimecres","Dijous","Divendres","Dissabte");

        foreach ($els as $el) {
            $da=date("m-d-Y", strtotime($el->data));
            $da2=$dias[date("w", strtotime($el->data))];
            
            $item=array("id"=>$el->id, "name"=>$el->user['name'], "data"=>$da2.", ".$da, "inici"=>$el->inici, "fi"=>$el->fi, "motiu"=>$el->motiu);
            array_push($ret, $item);
        }
        return $ret;
    }
    public function validacompensacio(Request $request)
    {
        //
        $el = compensa::where('id',$request->id)->update(['confirmat'=>true]);

        $compensa = compensa::find($request->id);
        if($compensa->inici == "09:00:00"){
            $m='matí';
        }else{
            $m='vesprada';
        }

        $link2 = "https://calendar.google.com/calendar/u/0/r/eventedit?text=COMPENSACIÓ+CEFIRE&dates=" . str_replace("-", "", $compensa->data) . "T" . str_replace(":", "", $compensa->inici) . "/" . str_replace("-", "", $compensa->data) . "T" . str_replace(":", "", $compensa->fi) . "&details=compensa+del+Cefire+de+Valencia+ELIMINADA&location=Valencia&trp=false#eventpage_6";
        $link3= "https://outlook.office.com/calendar/0/deeplink/compose?body=Guardia%20al%20CEFIRE&enddt=".$compensa->data."T".str_replace(":", "%3A", $compensa->fi)."&location=Valencia&path=%2Fcalendar%2Faction%2Fcompose&rru=addevent&startdt=".$compensa->data."T".str_replace(":", "%3A", $compensa->inici)."&subject=GUARDIA%20CEFIRE%20VALENCIA";


        $datos2 = [
            'nombre' => $compensa->user['name'],
            'fecha' => date("d/m/Y", strtotime($compensa->data)),
            'rato' => $m,
            'link' => $link2,
            'link' => $link3,
            'estat' => 'Aprovada'
        ];


        $emailJob3 = (new SendAviscompensacio($compensa->user['email'], $datos2))->delay(Carbon::now()->addSeconds(120));
        dispatch($emailJob3);

        //Mail::to($compensa->user['email'])->send(new Eliminarcompensa($datos2));

    }
    
}
